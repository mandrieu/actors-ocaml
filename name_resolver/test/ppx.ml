let () =
  Ppxlib.Driver.register_transformation_using_ocaml_current_ast
    "namer_resolver"
    ~impl:Name_resolver.impl
    ~intf:Name_resolver.intf;
  Ppxlib.Driver.standalone ()
