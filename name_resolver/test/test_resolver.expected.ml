let f ((x)[@defined "x_271"]) = ((x)[@resolved "x_271"])
let f ((x)[@defined "x_273"]) y =
  let x = (List.hd ((x)[@resolved "x_273"])) + y in x
let f ((x)[@defined "x_343"]) =
  object val y = ((x)[@resolved "x_343"]) method foo = y end
;;let ((x)[@defined "x_353"]) = 42 in
  fun _ -> ((x)[@resolved "x_353"]) + ((x)[@resolved "x_353"])
